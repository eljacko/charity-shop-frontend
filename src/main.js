import App from './App.vue'
import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueMaterial from 'vue-material'
import VueRouter from 'vue-router'
import axios from 'axios'
import 'vue-material/dist/theme/black-green-light.css'
import 'vue-material/dist/vue-material.min.css'

Vue.config.productionTip = false

axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';


Vue.use(VueRouter)
Vue.use(VueMaterial)
Vue.use(VueAxios, axios)

const router = new VueRouter({
})

window.vue = new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
