export default Object.freeze({
    ITEMS_PER_QUERY: 25,
    BASE_URL: 'http://localhost:8088/v1'
})
